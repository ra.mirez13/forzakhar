#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "maxminddb-compat-util.h"
#import "maxminddb.h"
#import "maxminddb_config.h"
#import "maxminddb_unions.h"
#import "MMDB.h"
#import "SimplePing.h"
#import "VPNManager.h"

FOUNDATION_EXPORT double VPNManagerVersionNumber;
FOUNDATION_EXPORT const unsigned char VPNManagerVersionString[];

