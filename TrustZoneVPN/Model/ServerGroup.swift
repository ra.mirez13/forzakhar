//
//  ServerGroup.swift
//  TrustZoneVPN
//
//  Created by Konstantin Chukhas on 8/2/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import Foundation
struct ServerGroup {
    let name: String
    let servers: [Server]
}
