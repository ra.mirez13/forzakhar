//
//  sslwrapper.c
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/19/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

/* C Version */
#include "sslwrapper.h"
#include <string.h>
//#include <jni.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/bio.h>
#include <openssl/x509.h>
#include <openssl/pkcs7.h>
#include <openssl/pkcs12.h>
#include <openssl/rc4.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include "cedar.h"
#include <time.h>
#include <wchar.h>
//#include <android/log.h>

/*
 * replace com_example_whatever with your package name
 *
 * HelloJni should be the name of the activity that will
 * call this function
 *
 * change the returned string to be one that exercises
 * some functionality in your wrapped library to test that
 * it all works
 *
 */


void dump(char * buf, void *d, UINT len)
{
    unsigned char *p = (unsigned char *)&d;
    int i;
    
    for (i = 0; i < len; i++)
    {
        if (i == 0)
        sprintf(buf, "%02x ", p[i]);
        else
        sprintf(buf+strlen(buf), "%02x ", p[i]);
    }
}

void openssl_lasterror(char *buf, const char *func, unsigned int len)
{
    char fmt[1024];
    unsigned long err;
    
    err = ERR_get_error();
    
    ERR_error_string_n(err, fmt, len);
    
    sprintf(buf, "FATALSSL (%s): %s", func, fmt);
}

void response_error(char *buf, const char *func, unsigned int len)
{
    sprintf(buf, "FATALRESPONSE: %s", func);
}

void sock_lasterror(char *buf, const char *func, unsigned int len)
{
    sprintf(buf, "FATALSOCKET (%s): [%d] %s", func, errno, strerror(errno));
}

int get_signature(char *buf, char *ip)
{
    unsigned char water_mark[] =
    {
        0x47, 0x49, 0x46, 0x38, 0x39, 0x61, 0xC8, 0x00, 0x33, 0x00, 0xF2, 0x00, 0x00, 0x36, 0x37, 0x34,
        0x79, 0x68, 0x54, 0x80, 0x80, 0x80, 0xAF, 0x7F, 0x5B, 0xB3, 0xA8, 0x9D, 0xD5, 0xD5, 0xD4, 0xFF,
        0xFF, 0xFF, 0x00, 0x00, 0x00, 0x2C, 0x00, 0x00, 0x00, 0x00, 0xC8, 0x00, 0x33, 0x00, 0x00, 0x03,
        0xFE, 0x08, 0x1A, 0xDC, 0x34, 0x0A, 0x04, 0x41, 0x6B, 0x65, 0x31, 0x4F, 0x11, 0x80, 0xF9, 0x60,
        0x28, 0x8E, 0x64, 0x69, 0x9E, 0x68, 0xAA, 0xAE, 0x6C, 0xEB, 0x9A, 0x4B, 0xE3, 0x0C, 0x0C, 0x25,
        0x6F, 0x56, 0xA7, 0xE9, 0xD2, 0xEB, 0xFF, 0xC0, 0xA0, 0x70, 0xC8, 0x8A, 0xDC, 0x2C, 0x9C, 0xC6,
        0x05, 0xC7, 0x31, 0x66, 0x24, 0x04, 0xA2, 0x74, 0x4A, 0xAD, 0x4E, 0x05, 0xB1, 0x0D, 0x61, 0xCB,
        0x25, 0xD4, 0xB8, 0x49, 0x1B, 0xE6, 0x19, 0xB1, 0x9A, 0xCF, 0xE8, 0xF4, 0x07, 0x2B, 0x11, 0x74,
        0x09, 0x85, 0x78, 0xFC, 0x0D, 0x6E, 0x90, 0x9F, 0xEA, 0x02, 0x81, 0x12, 0x35, 0xEF, 0x29, 0x6A,
        0x81, 0x2C, 0x04, 0x0A, 0x6E, 0x5C, 0x72, 0x88, 0x7A, 0x7A, 0x6F, 0x4D, 0x77, 0x19, 0x25, 0x71,
        0x16, 0x71, 0x2F, 0x05, 0x92, 0x06, 0x95, 0x80, 0x22, 0x48, 0x16, 0x7D, 0x98, 0x02, 0x9A, 0x7C,
        0x82, 0x06, 0x16, 0x23, 0x7F, 0x02, 0x05, 0x6B, 0x48, 0x70, 0x23, 0x15, 0x7D, 0x1F, 0x98, 0xA8,
        0x21, 0x7F, 0x87, 0x89, 0xB5, 0x8B, 0x7C, 0x7B, 0x3C, 0x8E, 0x23, 0x9E, 0x9B, 0xAE, 0x2B, 0xAD,
        0x20, 0xA6, 0xAC, 0x9B, 0x14, 0xB1, 0xC3, 0x21, 0x15, 0xB1, 0x81, 0x9E, 0x22, 0x9E, 0xAE, 0xC5,
        0x99, 0x20, 0x96, 0xAF, 0xC6, 0xA0, 0x70, 0xB6, 0xB6, 0x5B, 0x03, 0x1C, 0x16, 0x8E, 0x65, 0x21,
        0xBD, 0x9B, 0xCB, 0x2A, 0x9E, 0xCB, 0xC1, 0xE1, 0xD1, 0xA7, 0xA9, 0x6E, 0xE9, 0xD6, 0x82, 0xCD,
        0xC9, 0xCA, 0xD5, 0xD1, 0xAE, 0xBD, 0xCB, 0x7F, 0xAC, 0xB4, 0xD9, 0x73, 0x34, 0x37, 0x76, 0xDF,
        0x3C, 0xC8, 0x9A, 0x07, 0x42, 0x4E, 0x38, 0x4C, 0xAB, 0x0A, 0xFA, 0x12, 0x17, 0xEA, 0x52, 0x05,
        0x12, 0x0C, 0xDB, 0x35, 0xD3, 0xF3, 0xCE, 0xD9, 0x2C, 0x72, 0x13, 0xB7, 0x40, 0x22, 0xE8, 0xFE,
        0xB0, 0x61, 0xC7, 0x4F, 0xEC, 0x40, 0x7E, 0x94, 0xF6, 0x50, 0x13, 0x36, 0x83, 0xA8, 0x6A, 0x79,
        0xF9, 0x77, 0xE3, 0x1B, 0x28, 0x69, 0x1B, 0x55, 0x09, 0x1B, 0x67, 0x8A, 0x1A, 0xA9, 0x52, 0xC5,
        0x50, 0x71, 0x42, 0x82, 0x31, 0xDA, 0xB4, 0x56, 0x15, 0x9D, 0x71, 0xBC, 0x19, 0xF2, 0x27, 0x49,
        0x3E, 0xEF, 0x3C, 0x4E, 0xDB, 0x92, 0xED, 0x52, 0xBF, 0x01, 0xFE, 0x02, 0x44, 0x95, 0xB1, 0x6B,
        0xA0, 0x32, 0x72, 0x0A, 0x25, 0x72, 0x1C, 0xE5, 0x11, 0x99, 0x3C, 0x5F, 0x33, 0x61, 0x72, 0x75,
        0x93, 0x92, 0x28, 0x42, 0xA3, 0x7D, 0x72, 0x9A, 0x20, 0x68, 0x8A, 0x1C, 0x3A, 0x73, 0x3F, 0xE1,
        0x84, 0x82, 0x55, 0xEA, 0xE4, 0xA5, 0xBB, 0x89, 0xDE, 0x4C, 0x60, 0x30, 0x75, 0x0C, 0x9E, 0x97,
        0xD4, 0x8C, 0xC6, 0x32, 0x3B, 0xB4, 0x64, 0xD6, 0x71, 0x46, 0x45, 0x7E, 0x3C, 0x67, 0xB8, 0x30,
        0x20, 0xB8, 0x29, 0x82, 0x3D, 0x73, 0xE7, 0x93, 0x1E, 0xAA, 0x3F, 0x91, 0xD6, 0x89, 0x60, 0x9A,
        0xC8, 0x69, 0x36, 0xA8, 0x1B, 0xA4, 0xFE, 0x23, 0x03, 0x51, 0xED, 0xC7, 0xC4, 0x87, 0x19, 0xB7,
        0xA3, 0xCC, 0x13, 0x2D, 0x65, 0xD5, 0xB1, 0x22, 0x4A, 0xDE, 0xBA, 0xF6, 0xA1, 0x57, 0x7A, 0x0B,
        0xB3, 0x96, 0x3D, 0x95, 0xAF, 0x2E, 0x4A, 0xBC, 0x2A, 0xB9, 0x25, 0x61, 0x09, 0x10, 0x1C, 0x24,
        0x53, 0x7D, 0xBC, 0xA2, 0x33, 0xE0, 0x15, 0x72, 0x58, 0xC5, 0xAF, 0xAD, 0x8A, 0x84, 0x5C, 0x13,
        0xF1, 0xED, 0x13, 0xE6, 0x68, 0x57, 0x3F, 0x85, 0xB5, 0xF7, 0x58, 0xC3, 0xB2, 0x3A, 0xA7, 0x54,
        0xB9, 0x87, 0x86, 0x98, 0xBD, 0xA3, 0x8D, 0xD7, 0xCE, 0x44, 0xD4, 0xF1, 0x74, 0xDA, 0x44, 0x85,
        0x06, 0x25, 0x7C, 0x54, 0xEC, 0x57, 0xE8, 0x26, 0x18, 0xFE, 0x2A, 0xBA, 0xFE, 0xB9, 0xFE, 0xE6,
        0xCD, 0x88, 0x00, 0x57, 0x0B, 0x54, 0xFE, 0x20, 0x31, 0x1A, 0x0F, 0x01, 0x14, 0x94, 0xD0, 0x61,
        0x69, 0x95, 0x14, 0x0F, 0x3B, 0xAE, 0x5C, 0x37, 0x16, 0x56, 0xCF, 0xBD, 0x14, 0xA1, 0x61, 0x12,
        0x0E, 0xA6, 0x14, 0x76, 0x88, 0xBD, 0x44, 0xA1, 0x3C, 0xF6, 0x04, 0x76, 0x90, 0x78, 0xE4, 0x81,
        0x26, 0x80, 0x70, 0x0F, 0x10, 0xA7, 0xC4, 0x61, 0x95, 0x2D, 0xC6, 0x5C, 0x45, 0xCE, 0x89, 0x28,
        0x1B, 0x34, 0x1C, 0xC5, 0xE8, 0xD1, 0x64, 0xAF, 0xAC, 0xE2, 0x1C, 0x0A, 0xE2, 0xEC, 0xE7, 0x62,
        0x4C, 0xE4, 0xB4, 0x05, 0x51, 0x80, 0x93, 0x04, 0xE7, 0x8F, 0x70, 0x01, 0x6C, 0xA1, 0x62, 0x0D,
        0xFE, 0x75, 0xF8, 0xC1, 0x76, 0x3D, 0x55, 0x54, 0x5D, 0x27, 0xD1, 0xE0, 0x23, 0x13, 0x64, 0x3B,
        0x6E, 0x67, 0xCD, 0x8E, 0x28, 0x20, 0x51, 0x5A, 0x50, 0xF2, 0x45, 0x89, 0xDF, 0x2B, 0xB5, 0x78,
        0x26, 0x07, 0x17, 0x04, 0x8A, 0xE6, 0x46, 0x5F, 0x2C, 0x1D, 0x84, 0xDC, 0x24, 0xBC, 0x60, 0xD6,
        0x1D, 0x78, 0x1F, 0x25, 0xA4, 0xE5, 0x7F, 0x75, 0x5E, 0x66, 0x18, 0x97, 0x73, 0xF0, 0x01, 0xA7,
        0x84, 0x27, 0x88, 0x58, 0xA1, 0x09, 0xDE, 0xC5, 0x05, 0x09, 0x3F, 0x88, 0xA0, 0x79, 0x24, 0x54,
        0x0F, 0x80, 0xC6, 0x66, 0x07, 0xA2, 0x44, 0x2A, 0xE9, 0xA4, 0x23, 0x22, 0x3A, 0xC7, 0x36, 0x0D,
        0x0C, 0xD0, 0x28, 0x81, 0xA0, 0xB5, 0x44, 0xE9, 0xA7, 0xA0, 0xA2, 0x71, 0x52, 0x36, 0x70, 0xE8,
        0x25, 0x55, 0x9A, 0x9C, 0x46, 0xE5, 0x8F, 0x40, 0xA1, 0xB6, 0xEA, 0x6A, 0x10, 0xA3, 0x9E, 0x49,
        0x9E, 0x92, 0xA7, 0xA6, 0xCA, 0xA9, 0xA7, 0xAF, 0xE6, 0xAA, 0xEB, 0x0A, 0xA5, 0x4E, 0x99, 0x57,
        0x1D, 0xB5, 0x6E, 0x8A, 0xEA, 0x18, 0xBB, 0x16, 0x6B, 0xAC, 0x3E, 0x71, 0x20, 0xFE, 0x48, 0x16,
        0x36, 0x5D, 0x24, 0xC1, 0xA9, 0xB0, 0x69, 0xEA, 0x70, 0xEC, 0xB4, 0xC6, 0x26, 0xD9, 0x45, 0x0D,
        0x1C, 0x8C, 0x0A, 0x2C, 0x81, 0xD0, 0x76, 0x2A, 0x2D, 0xB5, 0xE0, 0xBE, 0x9A, 0xA4, 0x21, 0xB9,
        0x0C, 0x47, 0x6E, 0x9F, 0xB5, 0xDA, 0xEA, 0x28, 0xB1, 0x25, 0x88, 0x54, 0xD2, 0x98, 0x8D, 0xD5,
        0xA7, 0x09, 0x31, 0xF6, 0x25, 0x33, 0x4A, 0x48, 0x9F, 0x80, 0x34, 0xA6, 0x0A, 0x74, 0x56, 0xA1,
        0xAF, 0x0F, 0x6D, 0x10, 0x27, 0x41, 0x1B, 0x4C, 0x79, 0xA1, 0x2E, 0x5F, 0x9D, 0xAA, 0x67, 0xEF,
        0x1A, 0xD3, 0x30, 0xBC, 0xF0, 0xBD, 0xEE, 0xDE, 0xEB, 0x30, 0x57, 0xF3, 0x36, 0x4C, 0xC2, 0xBF,
        0x12, 0x5B, 0xBC, 0x6F, 0x97, 0x16, 0x9B, 0xB1, 0xB1, 0x0A, 0x59, 0xC8, 0x30, 0x9C, 0xC8, 0xDB,
        0x68, 0x9A, 0xEA, 0x02, 0x09, 0x2B, 0x70, 0x71, 0xC7, 0x15, 0xB3, 0x92, 0x71, 0xBE, 0x1A, 0x67,
        0x3C, 0xF1, 0x57, 0xF8, 0xC2, 0x6C, 0x14, 0xC4, 0xEE, 0xB2, 0x27, 0x33, 0xBC, 0x3A, 0xC3, 0x2C,
        0x2F, 0xC4, 0xEC, 0x8C, 0x25, 0xF1, 0xBB, 0xFD, 0x7E, 0x10, 0xB2, 0x12, 0xC4, 0x91, 0x5B, 0x32,
        0x54, 0x46, 0x14, 0xB7, 0xF2, 0xCC, 0x0F, 0xCF, 0x1B, 0x71, 0xC4, 0x40, 0x83, 0xF2, 0x30, 0xC6,
        0xFA, 0x92, 0x92, 0x35, 0xC3, 0x53, 0x43, 0x87, 0x5F, 0xD7, 0xA9, 0x70, 0xDD, 0xB0, 0xCE, 0x62,
        0x57, 0x6D, 0xF6, 0x98, 0x4D, 0x8B, 0x3C, 0x32, 0xD2, 0xE4, 0xA6, 0x8A, 0xB0, 0x5F, 0x4F, 0xCB,
        0x1C, 0x75, 0xCC, 0x65, 0x57, 0xBD, 0x2F, 0xD9, 0x43, 0x3B, 0xEC, 0xF5, 0xC4, 0xF9, 0x6A, 0xED,
        0x72, 0xCB, 0x36, 0xBF, 0x2C, 0xB8, 0x62, 0x7E, 0x9F, 0x2D, 0xF8, 0x08, 0x69, 0x87, 0xB1, 0xF6,
        0x3F, 0x6B, 0xAA, 0x0B, 0x9A, 0xC2, 0x7C, 0xB7, 0xFB, 0xF7, 0xE0, 0x63, 0xFE, 0xC7, 0x27, 0x35,
        0xDD, 0x18, 0xD3, 0x6D, 0x36, 0xD4, 0x72, 0x53, 0x1E, 0xF9, 0xD4, 0x1D, 0xDB, 0x1C, 0xF8, 0xE8,
        0x24, 0x2C, 0xB0, 0x44, 0x0E, 0x2C, 0x99, 0xDE, 0x6D, 0x9A, 0x90, 0xEF, 0x1C, 0x7A, 0xCB, 0x9E,
        0xBB, 0x1E, 0x35, 0xE9, 0x79, 0xCB, 0x9D, 0x39, 0xE9, 0xF0, 0x8E, 0xAD, 0x7B, 0xD8, 0x86, 0x53,
        0x0D, 0xC8, 0xBF, 0xA0, 0x73, 0x6E, 0x80, 0x12, 0x39, 0x9C, 0x27, 0x72, 0x07, 0x3A, 0xB4, 0xED,
        0x76, 0xEB, 0x5E, 0xC3, 0x44, 0xF8, 0x4D, 0xF1, 0xEE, 0x0D, 0xD8, 0xCD, 0x7A, 0xF7, 0xFD, 0xD0,
        0xEF, 0x1A, 0xE3, 0xFD, 0x12, 0xF5, 0x60, 0x07, 0xBD, 0xB3, 0xCF, 0xA2, 0xE3, 0x9D, 0xB9, 0x01,
        0xA6, 0x9F, 0x6E, 0x7C, 0x0D, 0x18, 0xE8, 0x60, 0x2D, 0xB4, 0xEC, 0x4E, 0x1E, 0x77, 0xB8, 0x81,
        0x7C, 0x9C, 0x06, 0xF1, 0x17, 0xD8, 0x60, 0x6E, 0x68, 0x03, 0x2F, 0xA0, 0x68, 0x54, 0x2A, 0x4B,
        0xFE, 0x3E, 0xFC, 0x6A, 0x90, 0x1F, 0x1A, 0xCA, 0x57, 0xBF, 0xD0, 0x98, 0x2B, 0x09, 0xF9, 0x03,
        0x80, 0x21, 0x6E, 0xD5, 0x3A, 0x00, 0x3A, 0x30, 0x0D, 0x04, 0xB4, 0x1F, 0x0E, 0x8E, 0xE0, 0x17,
        0x23, 0x48, 0xF0, 0x11, 0x67, 0x20, 0xDC, 0xF7, 0xDE, 0xF5, 0x3F, 0xF9, 0x79, 0x29, 0x52, 0x02,
        0x7C, 0x60, 0x1A, 0x70, 0x37, 0xBB, 0xB5, 0xC0, 0xEE, 0x7D, 0x21, 0x94, 0x42, 0x0A, 0x45, 0xE8,
        0xB1, 0xD8, 0xB9, 0x6E, 0x6B, 0xE0, 0x13, 0x9A, 0x0C, 0x59, 0x96, 0xB5, 0x9C, 0xD9, 0x50, 0x6C,
        0xBE, 0x3B, 0x4A, 0xE7, 0x58, 0x28, 0x0A, 0x12, 0x26, 0x06, 0x78, 0x61, 0xEB, 0x59, 0xE4, 0x7E,
        0xF8, 0xB9, 0xDD, 0xE1, 0xAC, 0x88, 0x65, 0xAB, 0x17, 0x0F, 0x03, 0x18, 0x33, 0x0D, 0xC6, 0xCE,
        0x87, 0x14, 0xAB, 0x98, 0x0D, 0xD9, 0x33, 0xC5, 0xC0, 0xD9, 0xAD, 0x55, 0x70, 0x3B, 0x5C, 0xE2,
        0x08, 0xA1, 0x27, 0xBB, 0xBC, 0x05, 0x6F, 0x73, 0xB6, 0xD3, 0x9C, 0x14, 0x61, 0x27, 0x3A, 0xC0,
        0x69, 0x11, 0x84, 0x97, 0x73, 0xA2, 0x17, 0x83, 0xB8, 0x3B, 0xAA, 0x0D, 0xF1, 0x8B, 0x50, 0x1C,
        0xE2, 0x15, 0xCF, 0xD8, 0xC3, 0x34, 0x96, 0x10, 0x86, 0x83, 0xAB, 0x21, 0x19, 0xBD, 0x37, 0x43,
        0x0E, 0xCE, 0x4E, 0x87, 0xE3, 0xA3, 0x63, 0xB8, 0x56, 0x28, 0xC8, 0x42, 0x82, 0xB0, 0x68, 0x86,
        0x4C, 0xA4, 0x22, 0x17, 0xC9, 0xC8, 0x46, 0x3A, 0xF2, 0x91, 0x90, 0x8C, 0xA4, 0x24, 0x75, 0x95,
        0x00, 0x00, 0x3B,
    };
    int len = 0;
    
    sprintf(buf,             "POST %s HTTP/1.1\r\n", "/vpnsvc/connect.cgi");
    sprintf(buf+strlen(buf), "Host: %s\r\n", ip);
    sprintf(buf+strlen(buf), "Content-Type: %s\r\n", "image/jpeg");
    sprintf(buf+strlen(buf), "Connection: %s\r\n", "Keep-Alive");
    sprintf(buf+strlen(buf), "Content-length: %d\r\n", (int)sizeof(water_mark));
    strcat(buf, "\r\n");
    len = strlen(buf);
    memcpy(buf+strlen(buf), water_mark, sizeof(water_mark));
    
    return len+sizeof(water_mark);
}

int pack_to_buf(PACK *p, char *ip, char *buf, UINT buf_size)
{
    BUF *b;
    time_t rawtime;
    struct tm * timeinfo;
    char strtime[100];
    int len;
    
    static const char wday_name[][4] = {
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };
    
    static const char mon_name[][4] = {
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };
    
    memset(buf,0,buf_size);
    
    time(&rawtime);
    timeinfo = localtime ( &rawtime );
    
    sprintf(strtime, "%s, %.2d %s %.4d %.2d:%.2d:%.2d", wday_name[timeinfo->tm_wday], timeinfo->tm_mday, mon_name[timeinfo->tm_mon], 1900 + timeinfo->tm_year, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
    
    sprintf(buf,             "POST %s HTTP/1.1\r\n", "/vpnsvc/vpn.cgi");
    sprintf(buf+strlen(buf), "Host: %s\r\n", ip);
    sprintf(buf+strlen(buf), "Date: %s\r\n", strtime);
    sprintf(buf+strlen(buf), "Content-Type: %s\r\n", "application/octet-stream");
    sprintf(buf+strlen(buf), "Keep-Alive: %s\r\n", "timeout=15; max=19");
    sprintf(buf+strlen(buf), "Connection: %s\r\n", "Keep-Alive");
    
    
    b = PackToBuf(p);
    
    sprintf(buf+strlen(buf), "Content-length: %d\r\n", (int)b->size);
    strcat(buf, "\r\n");
    
    len = strlen(buf);
    
    memcpy(buf+len, b->buf, b->size);
    len += b->size;
    
    FreeBuf(b);
    
    return len;
}

typedef struct ssl_ctx_st ssl_ctx_st;

typedef struct ssl_server {
    SSL *ssl;
    ssl_ctx_st *ctx;
    int sock;
    char server[16];
    char error[1024];
    int port;
} ssl_server;



bool server_connect(ssl_server *srv)
{
    const SSL_METHOD *method;
    struct sockaddr_in sockaddr4;
    char buf[1024*10];
    wchar_t uni_buf[1024*10];
    char *p;
    char *header;
    int len;
    int ret;
    BUF *b;
    PACK *pack;
    
    SSL_library_init();
    
    method = SSLv23_method();
    if (method == NULL)
    {
        openssl_lasterror(srv->error, "SSLv23_method", sizeof(srv->error));
        return false;
    }
    
    srv->ctx = SSL_CTX_new(method);
    if (srv->ctx == NULL)
    {
        openssl_lasterror(srv->error, "SSL_CTX_new", sizeof(srv->error));
        return false;
    }
    
    SSL_CTX_set_options(srv->ctx, SSL_OP_NO_TICKET);
    
    srv->sock = socket(AF_INET, SOCK_STREAM, 0);
    if (srv->sock == -1)
    {
        sock_lasterror(srv->error, "socket", sizeof(srv->error));
        SSL_CTX_free(srv->ctx);
        return false;
    }
    
    sockaddr4.sin_family=AF_INET;
    sockaddr4.sin_port=htons(srv->port);
    sockaddr4.sin_addr.s_addr=inet_addr(srv->server);
    
    ret = connect(srv->sock, (struct sockaddr*)&sockaddr4, sizeof(sockaddr4));
    if (ret != 0)
    {
        sock_lasterror(srv->error, "connect", sizeof(srv->error));
        close(srv->sock);
        SSL_CTX_free(srv->ctx);
        return false;
    }
    
    struct linger l;
    l.l_onoff = 1;
    l.l_linger = 10;
    if (setsockopt(srv->sock, SOL_SOCKET, SO_LINGER, &l, sizeof(l)) == -1) // SO_DONTLINGER
    {
        sock_lasterror(srv->error, "setsockopt SO_LINGER", sizeof(srv->error));
        close(srv->sock);
        SSL_CTX_free(srv->ctx);
        return false;
    }
    
    if (!SSL_CTX_set_ssl_version(srv->ctx, TLSv1_client_method()))
    {
        openssl_lasterror(srv->error, "SSL_CTX_set_ssl_version", sizeof(srv->error));
        close(srv->sock);
        SSL_CTX_free(srv->ctx);
        return false;
    }
    
    srv->ssl = SSL_new(srv->ctx);
    if (!srv->ssl)
    {
        openssl_lasterror(srv->error, "SSL_new", sizeof(srv->error));
        close(srv->sock);
        SSL_CTX_free(srv->ctx);
        return false;
    }
    
    if (!SSL_set_fd(srv->ssl, srv->sock))
    {
        openssl_lasterror(srv->error, "SSL_set_fd", sizeof(srv->error));
        SSL_free(srv->ssl);
        close(srv->sock);
        SSL_CTX_free(srv->ctx);
        return false;
    }
    
    if (SSL_connect(srv->ssl) <= 0)
    {
        openssl_lasterror(srv->error, "SSL_connect", sizeof(srv->error));
        SSL_free(srv->ssl);
        close(srv->sock);
        SSL_CTX_free(srv->ctx);
        return false;
    }
    
    SSL_set_mode(srv->ssl, SSL_MODE_AUTO_RETRY);
    SSL_set_mode(srv->ssl, SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER);
    
    len = get_signature(buf, srv->server);
    
    if (SSL_write(srv->ssl, buf, len) < 0)
    {
        openssl_lasterror(srv->error, "SSL_write", sizeof(srv->error));
        SSL_free(srv->ssl);
        close(srv->sock);
        SSL_CTX_free(srv->ctx);
        return false;
    }
    
    memset(buf,0,sizeof(buf));
    
    len = SSL_read(srv->ssl, buf, sizeof(buf));
    
    if (len < 0)
    {
        openssl_lasterror(srv->error, "SSL_read", sizeof(srv->error));
        SSL_free(srv->ssl);
        close(srv->sock);
        SSL_CTX_free(srv->ctx);
        return false;
    }
    
    header = strstr(buf,"\r\n\r\n");
    if (!header)
    {
        response_error(srv->error, "server hello: http header not found", sizeof(srv->error));
        SSL_free(srv->ssl);
        close(srv->sock);
        SSL_CTX_free(srv->ctx);
        return false;
    }
    
    header += 4;
    
    p = strstr(header, "error");
    if (p)
    {
        char fmt[1024];
        sprintf(fmt, "server hello: %s", p);
        response_error(srv->error, fmt, sizeof(srv->error));
        SSL_free(srv->ssl);
        close(srv->sock);
        SSL_CTX_free(srv->ctx);
        return false;
    }
    
    len -= header-buf;
    if (len < 0)
    {
        len = 0;
    }
    
    b = NewBuf();
    WriteBuf(b, header, len);
    SeekBuf(b, 0, 0);
    pack = BufToPack(b);
    
    if (!PackGetInt(pack,"version"))
    {
        FreePack(pack);
        FreeBuf(b);
        SSL_free(srv->ssl);
        close(srv->sock);
        SSL_CTX_free(srv->ctx);
        
        response_error(srv->error, "Server version is null", sizeof(srv->error));
        return false;
    }
    
    FreePack(pack);
    FreeBuf(b);
    return true;
}

void print_hex(char *buf, int buf_len, unsigned char *str, int str_len)
{
    size_t i,j,f_len;
    char f[10];
    
    memset(buf, 0, buf_len);
    memset(f, 0, sizeof(f));
    
    sprintf(f, "%s0%uX ", "%", sizeof(char)*2);
    f_len = sizeof(char)*2+1;
    
    for (i=0, j=0; j<buf_len && i<str_len; i++, j+=f_len)
    {
        sprintf(buf+j, f, (unsigned char)(*(str+i)));
    }
    
}
//MARK:SslLogin

int SslLogin( char * str, int strsize, char *email, char *email_password, char *lang)
{
    char buf[1024*10];
    wchar_t uni_tmp[1024*10];
    wchar_t *uni_p;
    char cipher_name[1024];
    char *header;
    char *p;
    int len;
    int code;
    BUF *b;
    PACK *pack;
    int i;
    //    char *email;
    //    char *lang;
    //    char *email_password;
    int obj_class;
    int id_user_name;
    int id_email_password;
    int id_password;
    int id_email;
    int id_servers;
    int id_error;
    int id_lang;
    ssl_server srv;
    //    char jstr_email;
    //    char jstr_email_password;
    //    char jstr_lang;
    unsigned char utf_buf[1024*10];
    
    
    srv.port = 443;
    StrCpy(srv.server, sizeof(srv.server), "148.251.151.35");
    
    if (!server_connect(&srv))
    {
        ALOG("Failed 6...");
        return false;
    }
    pack = NewPack();
    
    PackAddStr(pack, "method", "ulogin");
    PackAddStr(pack, "email", email);
    PackAddStr(pack, "pass", email_password);
    PackAddStr(pack, "lang", lang);
    PackAddStr(pack, "client_str", "IOS");
    PackAddInt(pack, "client_ver", 1);
    PackAddInt(pack, "client_build", 99999);
    
    //    (*env)->ReleaseStringUTFChars(env, jstr_email, email);
    //    (*env)->ReleaseStringUTFChars(env, jstr_email_password, email_password);
    //    (*env)->ReleaseStringUTFChars(env, jstr_lang, lang);
    
    len = pack_to_buf(pack, srv.server, buf, sizeof(buf));
    
    FreePack(pack);
    
    if (SSL_write(srv.ssl, buf, len) < 0)
    {
        openssl_lasterror(srv.error, "[login]: SSL_write", sizeof(srv.error));
        SSL_free(srv.ssl);
        close(srv.sock);
        SSL_CTX_free(srv.ctx);
        
        //        (*env)->SetObjectField(env, o, id_error, (*env)->NewStringUTF(env, srv.error));
        
        ALOG("Failed 10...");
        return false;
    }
    
    memset(buf,0,sizeof(buf));
    
    len = SSL_read(srv.ssl, buf, sizeof(buf));
    
    if (len < 0)
    {
        openssl_lasterror(srv.error, "[login]: SSL_read", sizeof(srv.error));
        SSL_free(srv.ssl);
        close(srv.sock);
        SSL_CTX_free(srv.ctx);
        
        //        (*env)->SetObjectField(env, o, id_error, (*env)->NewStringUTF(env, srv.error));
        
        ALOG("Failed 11...");
        return false;
    }
    else
    {
        char tmp[10240];
        
        memset(tmp,0,sizeof(tmp));
        
        for (int i=0;i < len; i++)
        {
            if (buf[i] == 0)
            tmp[i] = 32;
            else
            tmp[i] = buf[i];
        }
        
        ALOG("Server answer size=%d: '%s'", 2000, tmp);
        
    }
    
    header = strstr(buf,"\r\n\r\n");
    if (!header)
    {
        response_error(srv.error, "[login]: http header not found", sizeof(srv.error));
        SSL_free(srv.ssl);
        close(srv.sock);
        SSL_CTX_free(srv.ctx);
        //
        ////        (*env)->SetObjectField(env, o, id_error, (*env)->NewStringUTF(env, srv.error));
        //
        ALOG("Failed 12...");
        return false;
    }
    
    header += 4;
    
    p = strstr(header, "error");
    if (p)
    {
        char fmt[1024];
        sprintf(fmt, "[login]: %s", p);
        response_error(srv.error, fmt, sizeof(srv.error));
        SSL_free(srv.ssl);
        close(srv.sock);
        SSL_CTX_free(srv.ctx);
        //
        ////        (*env)->SetObjectField(env, o, id_error, (*env)->NewStringUTF(env, srv.error));
        //
        //
        ALOG("Failed 13...");
        return false;
    }
    
    len -= header-buf;
    if (len < 0)
    {
        len = 0;
    }
    
    b = NewBuf();
    WriteBuf(b, header, len);
    SeekBuf(b, 0, 0);
    pack = BufToPack(b);
    
    code = PackGetInt(pack,"code");
    if (code)
    {
        ALOG("Answer header '%s' code=%d...", header, code);
    }
    else
    {
        FreePack(pack);
        FreeBuf(b);
        SSL_free(srv.ssl);
        close(srv.sock);
        SSL_CTX_free(srv.ctx);
        
        response_error(srv.error, "[login]: failed, try again", sizeof(srv.error));
        
        //        (*env)->SetObjectField(env, o, id_error, (*env)->NewStringUTF(env, srv.error));
        
        ALOG("Failed 14...");
        return false;
    }
    
    memset(buf,0,sizeof(buf));
    
    if (PackGetUniStr(pack, "msg", uni_tmp, sizeof(uni_tmp)))
    {
        int wr;
        char hex[1024];
        
        ALOG("Msg found size=%u...", wcslen(uni_tmp));
        
        print_hex(hex, sizeof(hex), (unsigned char*)uni_tmp, wcslen(uni_tmp)*sizeof(wchar_t));
        ALOG("Msg hex %u '%s'", wcslen(uni_tmp)*sizeof(wchar_t), hex);
        
        wr = wcstombs(buf, uni_tmp, sizeof(buf));
        ALOG("Msg wcstombs result %d '%s'", wr, buf);
        
        
        if (swprintf(utf_buf, sizeof(uni_tmp)/sizeof(wchar_t), L"%ls", uni_tmp))
        {
            ALOG("Msg wprintf result '%s'", uni_buf);
            wcstombs(buf, utf_buf, sizeof(buf));
            ALOG("Msg wcstombs result '%s'", buf);
        }
        else
        {
            //            wcstombs(buf, uni_tmp, sizeof(buf));
            ALOG("Msg wprintf failed");
        }
        
    }
    else
    {
        ALOG("Msg not found...");
        //        swprintf(uni_buf, sizeof(uni_buf)/sizeof(wchar_t), L"%ls", L"");
    }
    
    FreePack(pack);
    FreeBuf(b);
    
    //    sprintf(cipher_name,"DONE. Cipher %s, ssl %s, NumZone %d",SSL_get_cipher(srv.ssl), SSL_get_version(srv.ssl), zone_num);
    
    SSL_free(srv.ssl);
    close(srv.sock);
    SSL_CTX_free(srv.ctx);
    
    
    UniToUtf8(utf_buf, sizeof(utf_buf), uni_tmp);
    //    (*env)->SetObjectField(env, o, id_error, (jstring)(*env)->NewStringUTF(env, (char *)utf_buf));
    
    //    uni_p = wcsstr(uni_tmp,L"|");
    //  if (uni_p == NULL)
    // {
    // ALOG("Wrong body...");
    //   return false;
    // }
    
    //*uni_p = 0;
    //uni_p ++;
    
    len = strlen(utf_buf)+1;
    memset(str, 0, strsize);
    
    if (len > strsize)
    StrCpy(str, strsize, utf_buf);
    else
    StrCpy(str, len, utf_buf);
    
    //UniToUtf8(utf_buf, sizeof(utf_buf), uni_tmp);
    //    (*env)->SetObjectField(env, o, id_user_name, (jstring)(*env)->NewStringUTF(env, (char *)utf_buf));
    
    // UniToUtf8(utf_buf, sizeof(utf_buf), uni_p);
    //    (*env)->SetObjectField(env, o, id_password, (jstring)(*env)->NewStringUTF(env, (char *)utf_buf));
    
    //    (*env)->SetObjectField(env, o, id_user_name, (jstring)(*env)->NewStringUTF(env, uni_tmp));
    //    (*env)->SetObjectField(env, o, id_password, (jstring)(*env)->NewStringUTF(env, uni_p));
    
    return true;
}

//jboolean


//MARK:GetServerList

int SslGetServerList(char * str, int strsize, char *user_name, char *lang)
{
    
    char buf[1024*10];
    wchar_t uni_buf[1024*10];
    wchar_t uni_tmp[1024];
    char cipher_name[1024];
    char *header;
    char *p;
    int len;
    int zone_num;
    BUF *b;
    PACK *pack;
    int i;
    unsigned char utf_buf[1024*10];
    unsigned char *utf_p;
    //jclass obj_class;
    // jfieldID id_user_name;
    // jfieldID id_servers;
    // jfieldID id_note;
    // jfieldID id_expire;
    // jfieldID id_download;
    // jfieldID id_upload;
    // jfieldID id_download_limit;
    // jfieldID id_upload_limit;
    // jfieldID id_error;
    // jfieldID id_lang;
    ssl_server srv;
    // jstring jstr_user_name;
    // jstring jstr_lang;
    UINT64 expire;
    SYSTEMTIME stm;
    printf("%s\n", str);
    ALOG("Entering %s...", "");
    
    srv.port = 443;
    StrCpy(srv.server, sizeof(srv.server), "148.251.151.35");
    
    ALOG("Connect to server");
    if (!server_connect(&srv))
    {
        ALOG("Connect to server failed");
        return false;
    }
    
    pack = NewPack();
    
    ALOG("Build pack");
    
    PackAddStr(pack, "method", "get_zone_uni");
    PackAddStr(pack, "user_name", user_name);
    PackAddStr(pack, "user_lang", lang);
    PackAddStr(pack, "client_str", "IOS");
    PackAddInt(pack, "client_ver", 1);
    PackAddInt(pack, "client_build", 99999);
    
    len = pack_to_buf(pack, srv.server, buf, sizeof(buf));
    
    FreePack(pack);
    
    ALOG("Send data to server");
    if (SSL_write(srv.ssl, buf, len) < 0)
    {
        openssl_lasterror(srv.error, "[get_zone_uni]: SSL_write", sizeof(srv.error));
        SSL_free(srv.ssl);
        close(srv.sock);
        SSL_CTX_free(srv.ctx);
        
        //    (*env)->SetObjectField(env, o, id_error, (*env)->NewStringUTF(env, srv.error));
        
        ALOG("Failed 14...");
        return false;
    }
    
    memset(buf,0,sizeof(buf));
    
    ALOG("Recv data from server");
    len = SSL_read(srv.ssl, buf, sizeof(buf));
    
    if (len < 0)
    {
        ALOG("SSL_read error...");
        openssl_lasterror(srv.error, "[get_zone_uni]: SSL_read", sizeof(srv.error));
        SSL_free(srv.ssl);
        close(srv.sock);
        SSL_CTX_free(srv.ctx);
        
        return false;
    }
    
    header = strstr(buf,"\r\n\r\n");
    if (!header)
    {
        ALOG("Header not found...");
        response_error(srv.error, "[get_zone_uni]: http header not found", sizeof(srv.error));
        SSL_free(srv.ssl);
        close(srv.sock);
        SSL_CTX_free(srv.ctx);
        
        
        return false;
    }
    
    header += 4;
    
    p = strstr(header, "error");
    if (p)
    {
        char fmt[1024];
        sprintf(fmt, "[get_zone_uni]: %s", p);
        
        ALOG("Error field not empty...");
        
        response_error(srv.error, fmt, sizeof(srv.error));
        SSL_free(srv.ssl);
        close(srv.sock);
        SSL_CTX_free(srv.ctx);
        printf("%s\n", str);
        
        
        // (*env)->SetObjectField(env, o, id_error, (*env)->NewStringUTF(env, srv.error));
        
        return false;
    }
    
    len -= header-buf;
    if (len < 0)
    {
        len = 0;
    }
    
    b = NewBuf();
    WriteBuf(b, header, len);
    SeekBuf(b, 0, 0);
    pack = BufToPack(b);
    
    zone_num = PackGetInt(pack,"NumZone");
    if (!zone_num)
    {
        
        ALOG("No zones...");
        
        FreePack(pack);
        FreeBuf(b);
        SSL_free(srv.ssl);
        close(srv.sock);
        SSL_CTX_free(srv.ctx);
        
        response_error(srv.error, "[get_zone_uni]: no zones", sizeof(srv.error));
        
        // (*env)->SetObjectField(env, o, id_error, (*env)->NewStringUTF(env, srv.error));
        
        return false;
    }
    
    memset(utf_buf, 0, sizeof(utf_buf));
    
    utf_p = utf_buf;
    for (i=0; i<zone_num; i++)
    {
        if (i == 0)
        {
            if (PackGetUniStrEx(pack, "Group", uni_tmp, sizeof(uni_tmp), i))
            {
                len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, uni_tmp);
            }
            else
            {
                len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, L"");
            }
        }
        else
        {
            if (PackGetUniStrEx(pack, "Group", uni_tmp, sizeof(uni_tmp), i))
            {
                len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, L"\n");
                utf_p += len;
                len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, uni_tmp);
            }
            else
            {
                len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, L"\n");
            }
        }
        
        ALOG("Group 1, len=%d, remain=%d", len, utf_buf+sizeof(utf_buf)-utf_p);
        
        utf_p += len;
        
        if (PackGetUniStrEx(pack, "Description", uni_tmp, sizeof(uni_tmp), i))
        {
            len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, L"|");
            utf_p += len;
            len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, uni_tmp);
        }
        else
        {
            len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, L"|");
        }
        
        ALOG("Group 2, len=%d, remain=%d", len, utf_buf+sizeof(utf_buf)-utf_p);
        
        utf_p += len;
        
        if (PackGetUniStrEx(pack, "Flag", uni_tmp, sizeof(uni_tmp), i))
        {
            len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, L"|");
            utf_p += len;
            len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, uni_tmp);
        }
        else
        {
            len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, L"|");
        }
        
        ALOG("Group 3, len=%d, remain=%d", len, utf_buf+sizeof(utf_buf)-utf_p);
        
        utf_p += len;
        
        if (PackGetUniStrEx(pack, "Name", uni_tmp, sizeof(uni_tmp), i))
        {
            len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, L"|");
            utf_p += len;
            len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, uni_tmp);
        }
        else
        {
            len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, L"|");
        }
        
        ALOG("Group 4, len=%d", len);
        
        utf_p += len;
        
        if (PackGetBoolEx(pack, "Available", i))
        {
            len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, L"|1");
        }
        else
        {
            len = UniToUtf8(utf_p, utf_buf+sizeof(utf_buf)-utf_p, L"|0");
        }
        
        ALOG("Group 5, len=%d", len);
        
        utf_p += len;
        
    }
    
    len = strlen(utf_buf);
    memset(str, 0, strsize);
    
    if (len > strsize)
    StrCpy(str, strsize, utf_buf);
    else
    StrCpy(str, len, utf_buf);
    
    //    memset(buf,0,sizeof(buf));
    //    wcstombs(buf, uni_buf, sizeof(buf));
    // (*env)->SetObjectField(env, o, id_servers, (jstring)(*env)->NewStringUTF(env, (char *)utf_buf));
    
    PackGetUniStr(pack, "Expire", uni_tmp, sizeof(uni_tmp));
    
    memset(buf,0,sizeof(utf_buf));
    expire = UniToInt64(uni_tmp);
    
    if (expire)
    {
        UINT64ToSystem(&stm, SystemToLocal64(expire));
        
        snprintf(buf, sizeof(buf)/sizeof(char), "%02d.%02d.%d %02d:%02d", stm.wDay, stm.wMonth, stm.wYear, stm.wHour, stm.wMinute);
        //        wcstombs(buf, uni_tmp, sizeof(buf));
        //   (*env)->SetObjectField(env, o, id_expire, (jstring)(*env)->NewStringUTF(env, (char *)buf));
    }
    else
    {
        // (*env)->SetObjectField(env, o, id_expire, (jstring)(*env)->NewStringUTF(env, (char *)utf_buf));
    }
    
    PackGetUniStr(pack, "Note", uni_tmp, sizeof(uni_tmp));
    
    memset(utf_buf,0,sizeof(utf_buf));
    
    
    UniToUtf8(utf_buf, sizeof(utf_buf), uni_tmp);
    //    memset(buf,0,sizeof(buf));
    //    wcstombs(buf, uni_tmp, sizeof(buf));
    // (*env)->SetObjectField(env, o, id_note, (jstring)(*env)->NewStringUTF(env, (char *)utf_buf));
    
    
    // (*env)->SetLongField(env, o, id_download, (jlong)PackGetInt64(pack, "TotalDownload"));
    //(*env)->SetLongField(env, o, id_upload, (jlong)PackGetInt64(pack, "TotalUpload"));
    // (*env)->SetLongField(env, o, id_upload_limit, (jlong)PackGetInt64(pack, "UploadLimit"));
    //(*env)->SetLongField(env, o, id_download_limit, (jlong)PackGetInt64(pack, "DownloadLimit"));
    
    
    FreePack(pack);
    FreeBuf(b);
    
    //    sprintf(cipher_name,"DONE. Cipher %s, ssl %s, NumZone %d",SSL_get_cipher(srv.ssl), SSL_get_version(srv.ssl), zone_num);
    
    SSL_free(srv.ssl);
    close(srv.sock);
    SSL_CTX_free(srv.ctx);
    
    return true;
    
    //  char name[200];
    //snprintf(name,  "%s %s", "Hello", "Swift!");
    //return strdup(name);
    
}

