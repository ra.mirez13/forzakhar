//
//  sslwrapper.h
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/15/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

#ifndef sslwrapper_h
#define sslwrapper_h
int SslLogin( char * str, int strsize, char *email, char *email_password, char *lang);
int  SslGetServerList(char * str, int strsize, char *user_name, char *lang);

#endif /* sslwrapper_h */
