//
//  ServerCollectionCell.swift
//  openSSlVPN
//
//  Created by Zakhar on 7/30/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit

class ServerCollectionCell: UICollectionViewCell {
    @IBOutlet private weak var flagImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet weak var viewBack: UIView!
    
    static var cellIdentifier: String {
        
        return String(describing: self)
    }
    
    static var cellNib: UINib {
        return UINib(nibName: "ServerCell", bundle: nil)
        
    }
   
   
    func setFlagImage(_ image: UIImage?) {
        flagImageView.image = image
//        flagImageView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        flagImageView.layer.masksToBounds = true
        viewBack.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        viewBack.layer.masksToBounds = true


    }
    
    func setName(_ name: String) {
        nameLabel.text = name
    }
}
extension UIImage {
    func imageWithInsets(insets: UIEdgeInsets) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: self.size.width + insets.left + insets.right,
                   height: self.size.height + insets.top + insets.bottom), false, self.scale)
        let _ = UIGraphicsGetCurrentContext()
        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithInsets
    }
}
