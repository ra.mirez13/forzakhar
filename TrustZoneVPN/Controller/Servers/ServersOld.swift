//
//  ServersVC.swift
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/18/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit

class ServersVCOld: UIViewController {
    
    struct Groups {
        let title : String
        let country:String
        let atributes:String
        let domen: String
        let number:Int
        
    }
    
    
    @IBOutlet weak var tableView: JNExpandableTableView!
    
    var array = [String]()
    var arrayGroups:[Groups] = []
    var arr = [(name:String, domen:[(String,String)])]()
    static fileprivate let kTableViewCellReuseIdentifier = "TableViewCellReuseIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getServer()
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
    }
    
    var titleVpn = [String]()
    var grouptitles = [String]()
    
    //MARK: Сет
    var set = Set<String> ()
    var exist = false
    func getServer()  {
        // GetServer
        var prevGroupTitle = ""
        
        let strsize = 10240;
        let str = UnsafeMutablePointer<Int8>.allocate(capacity: strsize);
        let unsafePointerUserName = UnsafeMutablePointer<Int8>(mutating: ("ADMIN" as NSString).utf8String)
        let unsafePointerLang = UnsafeMutablePointer<Int8>(mutating: ("EN" as NSString).utf8String)
        SslGetServerList(str,  Int32(strsize),unsafePointerUserName, unsafePointerLang)
        let s:String  = String(cString: str);      //это отправка в функцию значение
        
        let fullNameArr = s.components(separatedBy: "\n")
        
        for i in fullNameArr{
            let a = i.components(separatedBy: "|")
            let groupTitle = String(a[0])
            if (groupTitle != prevGroupTitle) {
                grouptitles.append(groupTitle)
            }
            
            arrayGroups.append(Groups(title: String(a[0]), country: String(a[1]), atributes: String(a[2]), domen: String(a[3]), number: 1))
            arr.append((name: groupTitle, domen: [(String(a[2]), String(a[3]))]))
            
            prevGroupTitle = groupTitle
            
        }
        
        
        //     print(arr)
        
    }
    
}
extension ServersVCOld:UITableViewDelegate,UITableViewDataSource,JNExpandableTableViewDelegate, JNExpandableTableViewDataSource{
    func tableView(_ tableView: JNExpandableTableView, canExpand indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: JNExpandableTableView, willExpand indexPath: IndexPath) {
        let headerView = self.tableView.cellForRow(at: indexPath) as? AccordionHeaderView
        headerView?.arrowIcon.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
    }
    
    func tableView(_ tableView: JNExpandableTableView, willCollapse indexPath: IndexPath) {
        // print("willCollapse")
        let headerView = self.tableView.cellForRow(at: indexPath) as? AccordionHeaderView
        headerView?.arrowIcon.transform = CGAffineTransform(rotationAngle: CGFloat(2*Double.pi))
        
    }
    //MARK:Table View Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath == self.tableView.expandedContentIndexPath {
            return 350.0
        } else {
            return 44.0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayGroups.count
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return JNExpandableTableViewNumberOfRowsInSection((tableView as? JNExpandableTableView)!, section, 1)
    }
    // static let tableViewCellIdentifier = "expandedCell"
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if self.tableView.expandedContentIndexPath == indexPath {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "expandedCell") as! TopTableViewCell
            
            return cell
        } else {
            let cell = Bundle.main.loadNibNamed("AccordionHeaderView", owner: nil, options: nil)![0] as! AccordionHeaderView
            
            cell.label.text = arrayGroups[indexPath.section].title
            return cell
        }
        
    }
    
}
extension ServersVCOld:UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayGroups.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionView", for: indexPath) as! CollectionViewCell
        
        cell.labelCollectionView.text = arrayGroups[indexPath.row].atributes
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .left)
        
        print("Выбрана ячейка: (\(indexPath.section), \(indexPath.item))")
    }
    
    
}
