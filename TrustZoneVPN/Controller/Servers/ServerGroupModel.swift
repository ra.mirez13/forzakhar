//
//  ServerSectionModel.swift
//  openSSlVPN
//
//  Created by Zakhar on 7/30/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import Foundation

struct ServerGroupCellModel {
    let group: ServerGroup
    var isExpanded: Bool = true
}
