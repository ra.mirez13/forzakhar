//
//  ServersVC.swift
//  openSSlVPN
//
//  Created by Zakhar on 7/30/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit

final class ServersVC: UIViewController {
    @IBOutlet private weak var collectionView: UICollectionView!
    
    private let serversService = ServersService()
    private var cellModels = [ServerGroupCellModel]()
    
    private let itemsInRow: CGFloat = 4
    
    private var itemSize: CGFloat = .zero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(ServerCollectionCell.cellNib, forCellWithReuseIdentifier: ServerCollectionCell.cellIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self

        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            guard let self = self else { return }
            self.cellModels = self.serversService.getServerGroups().map { ServerGroupCellModel(group: $0, isExpanded: true) }
            DispatchQueue.main.async { self.collectionView.reloadData() }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        itemSize = (collectionView.frame.size.width - itemsInRow * 3) / itemsInRow
    }
}

extension ServersVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return cellModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if !cellModels[section].isExpanded {
            return 0
        }
        
        return section < cellModels.count ? cellModels[section].group.servers.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: ServerCollectionCell.cellIdentifier, for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? ServerCollectionCell else { return }
        let server = cellModels[indexPath.section].group.servers[indexPath.row]
        cell.setName(server.domenName.uppercased())
        cell.setFlagImage(server.flag)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: itemSize, height: itemSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                           withReuseIdentifier: ServerGroupHeaderView.identifier,
                                                                           for: indexPath) as? ServerGroupHeaderView else { return UICollectionReusableView() }
        header.setGroupName(cellModels[indexPath.section].group.name)
        header.setArrowDirection(cellModels[indexPath.section].isExpanded ? .down : .up)
        header.headerTapAction = { [weak self] headerView in
            guard let self = self else { return }
            let isExpanded = !self.cellModels[indexPath.section].isExpanded
            self.cellModels[indexPath.section].isExpanded = isExpanded
            
            self.collectionView.performBatchUpdates({
                self.collectionView.reloadSections(IndexSet(integer: indexPath.section))
            }, completion: nil)
        }
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let domenName = cellModels[indexPath.section].group.servers[indexPath.row].serverDomen
        print(domenName)
        DomenName.sharedInstance.name = domenName
         _ = self.tabBarController?.selectedIndex = 2

       
    }
}

