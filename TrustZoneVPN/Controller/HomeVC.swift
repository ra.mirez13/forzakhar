//
//  HomeVC.swift
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/18/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit
import MBCircularProgressBar
import NetworkExtension
import KeychainSwift
import Security
import MaterialActivityIndicator
import QuartzCore
import Alamofire



class HomeVC: UIViewController {
    
    
    @IBOutlet weak var progressView: MBCircularProgressBarView!
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var switchConntectionStatus: UISwitch!
    @IBOutlet weak var labelConntectionStatus: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var materialActivityIndicator: MaterialActivityIndicatorView!
    @IBOutlet weak var touchLabel: UILabel!
    @IBOutlet weak var ipLabel: UILabel!
    @IBOutlet weak var ipImage: UIImageView!
    
    
    
    var vpnManager = NEVPNManager.shared()
    var isConnected = false
    var buttonSwitched : Bool = false
    var networkManager = NetworkManager()
    var servLine = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.VPNStatusDidChange(_:)), name: NSNotification.Name.NEVPNStatusDidChange, object: nil)
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
        
    }
    
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getIP()
        if self.img.image != nil{
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                let yourImage: UIImage =  UIImage(named: "ic_launcher")!
                self.img.image =  yourImage
                self.img?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            })
        }else{
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                let yourImage: UIImage = UIImage(named: "ic_launcher")!
                self.img.image =  yourImage
                self.img?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            })
        }
       
        
        let keychain = KeychainSwift()
        keychain.set(UserDefaults.standard.object(forKey: "password") as! String, forKey: "vpnPassword")
    }
    func getIP()  {
        NetworkManager.sharedInstance.aa { (result) in
            self.servLine.append(result! as! String)
            for line in self.servLine {
                let serverProps = line.components(separatedBy: "|")
                if serverProps.count < 1 { continue }
                self.ipLabel.text = serverProps[0]
                UserDefaults.standard.set(serverProps[0], forKey: "serverIp")
                let image: UIImage = UIImage(named:"\(serverProps[1].lowercased())flags")!
                self.ipImage.image = image
            }
        }
    }
    
    @objc func pressButton(_ sender: UIButton){
        //        if activ == true{
        //              materialActivityIndicator.startAnimating()
        //        }else{
        //              materialActivityIndicator.stopAnimating()
        //        }
        
    }
    func switchClicked() {
        
        switchConntectionStatus.isOn = true
        
        if !isConnected {
            initVPNTunnelProviderManager()
        }
        else{
            vpnManager.removeFromPreferences(completionHandler: { (error) in
                
                if((error) != nil) {
                    print("VPN Remove Preferences error: 1")
                    self.materialActivityIndicator.startAnimating()
                }
                else {
                    self.vpnManager.connection.stopVPNTunnel()
                    self.labelConntectionStatus.text = "Disconnected"
                    self.switchConntectionStatus.isOn = false
                    self.isConnected = false
                }
            })
        }
    }
    func initVPNTunnelProviderManager(){
        
        self.vpnManager.loadFromPreferences { (error) -> Void in
            
            if((error) != nil) {
                print("VPN Preferences error: 1")
            }
            else {
                
                let p = NEVPNProtocolIKEv2()
                // You can change Protocol and credentials as per your protocol i.e IPSec or IKEv2
                p.username = "Qktvg"
                p.remoteIdentifier = "trust.zone"
                p.serverAddress = DomenName.sharedInstance.name ?? "vpn.trust.zone"
                p.useExtendedAuthentication = true
                p.disconnectOnSleep = false
                
                
                
                let keychain = KeychainSwift()
                let data = keychain.getData("vpnPassword")
                let pass = "49bbLaXt"
                p.passwordReference = data
                p.identityData =  Data(base64Encoded: "")
                p.authenticationMethod = NEVPNIKEAuthenticationMethod.none
                p.enablePFS = true
                
                p.sharedSecretReference = keychain.getData("vpnPassword")!
                // Useful for when you have IPSec Protocol
                
                
                
                self.vpnManager.protocolConfiguration = p
                self.vpnManager.isEnabled = true
                
                self.vpnManager.saveToPreferences(completionHandler: { (error) -> Void in
                    if((error) != nil) {
                        print("VPN Preferences error: 2")
                    }
                    else {
                        
                        
                        self.vpnManager.loadFromPreferences(completionHandler: { (error) in
                            
                            if((error) != nil) {
                                
                                print("VPN Preferences error: 2")
                            }
                            else {
                                
                                var startError: NSError?
                                
                                do {
                                    try self.vpnManager.connection.startVPNTunnel()
                                }
                                catch let error as NSError {
                                    startError = error
                                    print(startError)
                                }
                                catch {
                                    print("Fatal Error")
                                    fatalError()
                                }
                                if((startError) != nil) {
                                    print("VPN Preferences error: 3")
                                    let alertController = UIAlertController(title: "Oops..", message:
                                        "Something went wrong while connecting to the VPN. Please try again.", preferredStyle: UIAlertController.Style.alert)
                                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default,handler: nil))
                                    
                                    
                                    
                                    self.present(alertController, animated: true, completion: nil)
                                    print(startError)
                                }
                                else {
                                    self.VPNStatusDidChange(nil)
                                    
                                    print("VPN started successfully..")
                                }
                                
                            }
                            
                        })
                        
                    }
                })
            }
        }
    }
    
    @objc func VPNStatusDidChange(_ notification: Notification?) {
        
        print("VPN Status changed:")
        let status = self.vpnManager.connection.status
        switch status {
        case .connecting:
            print("Connecting...")
            self.labelConntectionStatus.text = "Connecting..."
            self.switchConntectionStatus.isOn = false
            self.isConnected = false
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                let yourImage: UIImage = UIImage(named: "ic_launcher")!
                self.img.image = yourImage
                self.img?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                UserDefaults.standard.set("ic_launcher", forKey: "ImageDefaults")
            })
            
            //gradient
            materialActivityIndicator.addGradientWithColor(color: #colorLiteral(red: 0.2509803922, green: 0.7529411765, blue: 1, alpha: 1))
            materialActivityIndicator.layer.cornerRadius = 125
            materialActivityIndicator.startAnimating()
            materialActivityIndicator.rotate(duration: 3.0)
            
            materialActivityIndicator.color = #colorLiteral(red: 0.3103698492, green: 0.7292534709, blue: 0.9948167205, alpha: 1)
            materialActivityIndicator.lineWidth =  90
            materialActivityIndicator.layer.cornerRadius = 125
            materialActivityIndicator.isHidden = false
            touchLabel.text = "Touch to leave Trust.Zone"
            
            break
        case .connected:
            print("Connected")
            self.labelConntectionStatus.text = "Connected"
            self.switchConntectionStatus.isOn = true
            self.isConnected = true
            materialActivityIndicator.stopAnimating()
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                let yourImage: UIImage = UIImage(named: "logo_done")!
                self.img.image = yourImage
                self.img?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                UserDefaults.standard.set("logo_done", forKey: "ImageDefaults")
            })
            materialActivityIndicator.isHidden = true
            touchLabel.text = "Touch to leave Trust.Zone"
            NetworkManager.sharedInstance.aa { (result) in
                self.servLine.append(result! as! String)
                for line in self.servLine {
                    let serverProps = line.components(separatedBy: "|")
                    if serverProps.count < 1 { continue }
                    self.ipLabel.text = serverProps[0]
                    let image: UIImage = UIImage(named:"\(serverProps[1].lowercased())flags")!
                    self.ipImage.image = image
                }
            }
            VPNMethod.share.vpnStatusImage = true
            
            break
        case .disconnecting:
            print("Disconnecting...")
            self.labelConntectionStatus.text = "Disconnecting..."
            self.switchConntectionStatus.isOn = false
            self.isConnected = false
            materialActivityIndicator.stopAnimating()
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                let yourImage: UIImage = UIImage(named: "ic_launcher")!
                self.img.image = yourImage
                self.img?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                UserDefaults.standard.set("ic_launcher", forKey: "ImageDefaults")
            })
            materialActivityIndicator.isHidden = true
            
            break
        case .disconnected:
            print("Disconnected")
            self.labelConntectionStatus.text = "Disconnected..."
            self.switchConntectionStatus.isOn = false
            self.isConnected = false
            materialActivityIndicator.stopAnimating()
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                let yourImage: UIImage = UIImage(named: "logo_fail")!
                self.img.image = yourImage
                self.img?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                UserDefaults.standard.set("logo_fail", forKey: "ImageDefaults")
            })
            materialActivityIndicator.isHidden = true
            touchLabel.text = "Touch to enter Trust.Zone"
            NetworkManager.sharedInstance.aa { (result) in
                self.servLine.append(result! as! String)
                for line in self.servLine {
                    let serverProps = line.components(separatedBy: "|")
                    if serverProps.count < 1 { continue }
                    self.ipLabel.text = serverProps[0]
                    let image: UIImage = UIImage(named:"\(serverProps[1].lowercased())flags")!
                    self.ipImage.image = image
                }
            }
            
            
            break
        case .invalid:
            print("Invalid")
            self.labelConntectionStatus.text = "Invalid Connection"
            self.switchConntectionStatus.isOn = false
            self.isConnected = false
            materialActivityIndicator.stopAnimating()
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                let yourImage: UIImage = UIImage(named: "ic_launcher")!
                self.img.image = yourImage
                self.img?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                UserDefaults.standard.set("ic_launcher", forKey: "ImageDefaults")
            })
            materialActivityIndicator.isHidden = true
            touchLabel.text = "Touch to enter Trust.Zone"
            NetworkManager.sharedInstance.aa { (result) in
                self.servLine.append(result! as! String)
                for line in self.servLine {
                    let serverProps = line.components(separatedBy: "|")
                    if serverProps.count < 1 { continue }
                    self.ipLabel.text = serverProps[0]
                    let image: UIImage = UIImage(named:"\(serverProps[1].lowercased())flags")!
                    self.ipImage.image = image
                }
            }
            break
        case .reasserting:
            print("Reasserting...")
            self.labelConntectionStatus.text = "Reasserting Connection"
            self.switchConntectionStatus.isOn = false
            self.isConnected = false
            
            
            break
        }
    }
    
    @IBAction func pressButtonAction(_ sender: Any) {
        switchClicked()
        //
       
        materialActivityIndicator.addGradientWithColor(color: #colorLiteral(red: 0.2509803922, green: 0.7529411765, blue: 1, alpha: 1))
        materialActivityIndicator.layer.cornerRadius = 125
        materialActivityIndicator.startAnimating()
        materialActivityIndicator.rotate(duration: 3.0)
        
        materialActivityIndicator.color = #colorLiteral(red: 0.3103698492, green: 0.7292534709, blue: 0.9948167205, alpha: 1)
        materialActivityIndicator.lineWidth =  90
        materialActivityIndicator.layer.cornerRadius = 125
        materialActivityIndicator.isHidden = false
        //        }
        //        else
        //        {
        //            //your opposite UI styling
        //            print("your opposite UI styling")
        ////            UIView.animate(withDuration: 0.5, animations: {() -> Void in
        ////                self.img?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        ////            })
        //            //gradient
        //            materialActivityIndicator.isHidden = true
        //            materialActivityIndicator.deleteAddGradientWithColor(color: UIColor.white)
        //            materialActivityIndicator.layer.cornerRadius = 125
        //            materialActivityIndicator.stopRotating()
        //            materialActivityIndicator.stopAnimating()
        //            materialActivityIndicator.color = UIColor.white
        //            materialActivityIndicator.backgroundColor = UIColor.white
        //        }
        
    }
    
    
}
