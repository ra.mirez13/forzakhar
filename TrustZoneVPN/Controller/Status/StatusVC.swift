//
//  StatusVC.swift
//  TrustZoneVPN
//
//  Created by Konstantin Chukhas on 8/2/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit


class StatusVC: UIViewController {
    
    
    @IBOutlet weak var vpnStatusLabel: UILabel!
    @IBOutlet weak var serverIP: UILabel!
    @IBOutlet weak var login: UILabel!

    @IBOutlet weak var vpnStatusImage: UIImageView!
    @IBOutlet weak var logoutButton: UIButton!
    
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15.0),
        NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.6677469611, green: 0.6677629352, blue: 0.6677542925, alpha: 1),
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    var userDefaults = UserDefaults.standard
    var attributedString = NSMutableAttributedString(string:"")
    override func viewDidLoad() {
        super.viewDidLoad()
        //Button Logout
        let buttonTitleStr = NSMutableAttributedString(string:"(Logout)", attributes:attrs)
        attributedString.append(buttonTitleStr)
        logoutButton.setAttributedTitle(attributedString, for: .normal)
        
        // Do any additional setup after loading the view.
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
        self.login.text = (userDefaults.object(forKey: "login") as! String)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if VPNMethod.share.vpnStatusImage == true{
            let image: UIImage = UIImage(named:"online")!
            self.vpnStatusImage.image = image
            VPNMethod.share.vpnStatusLabel = "VPN ONLINE"
            
            self.vpnStatusLabel.text = VPNMethod.share.vpnStatusLabel
            self.vpnStatusLabel.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
//            self.serverIP.text = DomenName.sharedInstance.name ?? "vpn.trust.zone"

            
        }
    }
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
    }
    
    @IBAction func RenewAction(_ sender: Any) {
        let pth = "https://trust.zone/order"
        if let url = URL(string: "\(pth)"), !url.absoluteString.isEmpty {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
}
