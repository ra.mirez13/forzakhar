//
//  TabBarViewController.swift
//  TrustZoneVPN
//
//  Created by Konstantin Chukhas on 8/2/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import Foundation
//
//  TabBarVC.swift
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/18/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import Foundation

import UIKit


class TabBarController: UITabBarController {
    let button = UIButton.init(type: .custom)
    let tabBarView = UITableView()
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        self.selectedIndex = 2
        let btnImage = UIImage(named: "btn3")
        button.setTitleColor(.black, for: .normal)
        button.setImage(btnImage, for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
        button.isUserInteractionEnabled = false
        self.view.insertSubview(button, aboveSubview: self.tabBar)
        
        
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().backgroundColor = UIColor.clear
        UITabBar.appearance().backgroundImage = UIImage(named: "bg")
        UITabBar.appearance().contentMode = .scaleAspectFit
        UITabBar.appearance().clipsToBounds = true
        
        let appearance = UITabBarItem.appearance()
        let attributes = [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 13)]
        appearance.setTitleTextAttributes(attributes as [NSAttributedString.Key : Any], for: .normal)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        button.frame = CGRect.init(x: self.tabBar.center.x - 25, y: self.view.bounds.height - 45, width: 40  , height: 40)
        button.layer.cornerRadius = 20
        
    }
}
extension TabBarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return MyTransition(viewControllers: tabBarController.viewControllers)
    }
}

class MyTransition: NSObject, UIViewControllerAnimatedTransitioning {
    
    let viewControllers: [UIViewController]?
    let transitionDuration: Double = 0.3
    
    init(viewControllers: [UIViewController]?) {
        self.viewControllers = viewControllers
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return TimeInterval(transitionDuration)
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            let fromView = fromVC.view,
            let fromIndex = getIndex(forViewController: fromVC),
            let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
            let toView = toVC.view,
            let toIndex = getIndex(forViewController: toVC)
            else {
                transitionContext.completeTransition(false)
                return
        }
        
        let frame = transitionContext.initialFrame(for: fromVC)
        var fromFrameEnd = frame
        var toFrameStart = frame
        fromFrameEnd.origin.x = toIndex > fromIndex ? frame.origin.x - frame.width : frame.origin.x + frame.width
        toFrameStart.origin.x = toIndex > fromIndex ? frame.origin.x + frame.width : frame.origin.x - frame.width
        toView.frame = toFrameStart
        
        DispatchQueue.main.async {
            transitionContext.containerView.addSubview(toView)
            UIView.animate(withDuration: self.transitionDuration, animations: {
                fromView.frame = fromFrameEnd
                toView.frame = frame
            }, completion: {success in
                fromView.removeFromSuperview()
                transitionContext.completeTransition(success)
            })
        }
    }
    
    func getIndex(forViewController vc: UIViewController) -> Int? {
        guard let vcs = self.viewControllers else { return nil }
        for (index, thisVC) in vcs.enumerated() {
            if thisVC == vc { return index }
        }
        return nil
    }
}
