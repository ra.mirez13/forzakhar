//
//  SettingsVC.swift
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/18/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit
import BEMCheckBox

class SettingsVC: UIViewController {
    
    @IBOutlet weak var firstCB: BEMCheckBox!
    @IBOutlet weak var secondCB: BEMCheckBox!
    @IBOutlet weak var thirdCB: BEMCheckBox!
    @IBOutlet weak var forCB: BEMCheckBox!
    @IBOutlet weak var fiveCB: BEMCheckBox!
    var CheckBoxesArray: [BEMCheckBox]?
    override func viewDidLoad() {
        super.viewDidLoad()
        CheckBoxesArray = [firstCB,secondCB,thirdCB,forCB,fiveCB]
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
    }
    
    
    
    
}
extension SettingsVC: BEMCheckBoxDelegate {
    
    
    func didTap(_ checkBox: BEMCheckBox) {
        let selectedIntex = checkBox.tag
        guard let CheckBoxesArray = CheckBoxesArray else { return }
        for box in CheckBoxesArray where box.tag != selectedIntex {
            box.on = false
            
            switch selectedIntex{
            case 0:
                print("0")
            case 1:
                print("1")
            case 2:
                print("2")
            case 3:
                print("3")
            case 4:
                print("4")
            default:
                break
            }
            
        }
        
        
    }
    
}
